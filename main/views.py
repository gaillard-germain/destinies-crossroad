from django.shortcuts import render, redirect
from django.views import View
from django.http import JsonResponse
from django.template.loader import render_to_string
from .models import Card
from .forms import CharacterCreationForm
from registration.models import User


class CharacterCreation(View):
    """ Character creation view """

    form_class = CharacterCreationForm
    template_name = 'main/character_creation.html'

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST or None, request.FILES or None)

        if form.is_valid():
            character = form.save(commit=False)
            character.user = User.objects.get(id=request.user.id)
            character.save()

            return redirect('main:board', character_id=character.id)

        context = {'form': form}

        return render(request, self.template_name, context)

    def get(self, request, *args, **kwargs):
        return self.post(request, *args, **kwargs)


class DeleteCharacter(View):
    """ Remove the seleted character """

    def post(self, request, *args, **kwargs):
        response = {}
        character_id = request.POST.get('character_id')

        character = request.user.characters.get(id=character_id)
        name = '{} {}'.format(character.firstname, character.lastname)

        character.delete()

        response['msg'] = 'Deleted {}'.format(name)

        return render(request, 'home/character_selection.html')


class Board(View):
    """ The game board view """

    template_name = 'main/board.html'

    def get(self, request, character_id, *args, **kwargs):
        if request.user.is_authenticated:
            character = request.user.characters.get(id=character_id)
            ground = character.cards.filter(on_ground__gt=0)
            if not ground:
                character.fill_ground()

            context = {'character': character}

            return render(request, self.template_name, context)

        else:
            return redirect('registration:login')


class CardClick(View):
    """ Triggered when a card has been clicked """

    def post(self, request, *args, **kwargs):
        character_id = request.POST.get('character_id')
        card_id = request.POST.get('card_id')
        is_in = request.POST.get('is_in')

        card = Card.objects.get(id=card_id)
        character = request.user.characters.get(id=character_id)

        response = card.play(character, is_in)
        response['render'] = render_to_string(
            'main/character_snippet.html',
            {'character': character}
        )

        return JsonResponse(response)


class Move(View):
    """ Triggered when move button pressed """

    def post(self, request, *args, **kwargs):
        character_id = request.POST.get('character_id')
        character = request.user.characters.get(id=character_id)
        character.move()

        response = {
            'character': render_to_string(
                'main/character_snippet.html',
                {'character': character}
            ),
            'hand': render_to_string(
                'main/card_snippet.html',
                {'cards': character.get_hand()}
            ),
            'ground': render_to_string(
                'main/card_snippet.html',
                {'cards': character.get_ground()}
            ),
            'msg': '{} moved...'.format(character.firstname.title())
        }

        return JsonResponse(response)
