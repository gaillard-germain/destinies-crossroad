from django.contrib import admin
from .models import (Category, Effect, Card, CardEffect, Character,
                     CharacterCard)


class CardEffectInLine(admin.TabularInline):
    model = CardEffect
    extra = 1


@admin.register(Card)
class CardAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'category',
        'value',
        'image',
        'description',
        'fx',
        'cumulative',
        'enduring'
    )
    inlines = [CardEffectInLine, ]

    def fx(self, obj):
        string = [
            x.__str__() for x in CardEffect.objects.filter(card_id=obj.id)
        ]
        return ', '.join(string)


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ('name',)


@admin.register(Effect)
class EffectAdmin(admin.ModelAdmin):
    list_display = ('name',)


@admin.register(CardEffect)
class CardEffectAdmin(admin.ModelAdmin):
    model = CardEffect


class CharacterCardInLine(admin.TabularInline):
    model = CharacterCard
    extra = 0


@admin.register(Character)
class CharacterAdmin(admin.ModelAdmin):
    model = Character
    inlines = [CharacterCardInLine, ]


@admin.register(CharacterCard)
class CharacterCardAdmin(admin.ModelAdmin):
    model = CharacterCard
