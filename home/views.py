from django.shortcuts import render, redirect
from django.views import View


class Index(View):
    """ A view to display general information """

    template_name = 'home/index.html'

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            context = {}
            return render(request, self.template_name, context)
        else:
            return redirect('registration:login')
