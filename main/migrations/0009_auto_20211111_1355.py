# Generated by Django 3.2.8 on 2021-11-11 12:55

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0008_character_experience'),
    ]

    operations = [
        migrations.CreateModel(
            name='Zone',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('board', models.ManyToManyField(blank=True, related_name='boards', to='main.Card')),
            ],
        ),
        migrations.AddField(
            model_name='character',
            name='zone',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='characters', to='main.zone'),
        ),
    ]
