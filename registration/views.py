from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate
from django.views import View
from .forms import SignUpForm


class SignUp(View):
    """ A view to register a new user """

    form_class = SignUpForm
    template_name = 'registration/signup.html'

    def post(self, request, *args, **kwargs):

        form = self.form_class(request.POST or None)

        if form.is_valid():
            user = form.save()

            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=user.username, password=raw_password)
            login(request, user)

            return redirect('home:index')

        context = {'form': form}

        return render(request, self.template_name, context)

    def get(self, request, *args, **kwargs):
        return self.post(request, *args, **kwargs)
