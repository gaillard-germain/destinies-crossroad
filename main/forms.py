from django import forms
from .models import Character


class CharacterCreationForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['strenght'] = forms.IntegerField(
            initial=1,
            widget=forms.TextInput(attrs={'class': "attr-pts",
                                          'readonly': True})
        )
        self.fields['dexterity'] = forms.IntegerField(
            initial=1,
            widget=forms.TextInput(attrs={'class': "attr-pts",
                                          'readonly': True})
        )
        self.fields['intellect'] = forms.IntegerField(
            initial=1,
            widget=forms.TextInput(attrs={'class': "attr-pts",
                                          'readonly': True})
        )

    class Meta:
        model = Character
        fields = ('firstname', 'lastname', 'image', 'strenght', 'dexterity',
                  'intellect')
