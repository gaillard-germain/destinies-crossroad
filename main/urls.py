from django.urls import path, re_path
from . import views


app_name = 'main'

urlpatterns = [
    re_path(
        r'board/(?P<character_id>\d+)$',
        views.Board.as_view(),
        name='board'
    ),
    path(
        'card_click',
        views.CardClick.as_view(),
        name='card_click'
    ),
    path(
        'character_creation',
        views.CharacterCreation.as_view(),
        name='character_creation'
    ),
    path(
        'delete_character',
        views.DeleteCharacter.as_view(),
        name='delete_character'
    ),
    path(
        'move',
        views.Move.as_view(),
        name='move'
    )
]
